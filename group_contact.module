<?php

/**
 * Implements hook_menu().
 */
function group_contact_menu() {
  // Contact tab on group node.
  $items['node/%node/contact'] = array(
    'title' => 'Contact',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('group_contact_form'),
    'access callback' => 'group_contact_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function group_contact_permission() {
  // Access group contact form permission.
  $permission = array(
    'access group contact form' => array(
      'title' => t('Access group contact form'),
      'description' => t('Allow contacting group members via email through the Contact tab on the group page.'),
    ),
  );
  return $permission;
}

/**
 * Check if user can view group contact form.
 */
function group_contact_access($node) {
  // Determine if the user has access to the group contact form.
  if (og_user_access('node', $node->nid, 'access og contact form')) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Group contact form definition.
 */
function group_contact_form($form, &$form_state) {
  global $user;
  $nid = arg(1);
  $gid = $nid;

  if ($gid && arg(2) == 'contact') {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Your name'),
      '#maxlength' => 255,
      '#default_value' => $user->uid ? format_username($user) : '',
      '#required' => TRUE,
    );
    $form['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('Your email address'),
      '#maxlength' => 255,
      '#default_value' => $user->uid ? $user->mail : '',
      '#required' => TRUE,
    );
    $form['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#maxlength' => 255,
      '#required' => TRUE,
    );
    $form['message'] = array(
      '#type' => 'text_format',
      '#title' => t('Message'),
      '#format' => filter_default_format(),
    );
    $form['gid'] = array(
      '#type' => 'hidden',
      '#value' => $gid,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Send email'),
    );
  }
  return $form;
}

/**
 * Validate the group contact page form submission.
 */
function group_contact_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!valid_email_address($values['mail'])) {
    form_set_error('mail', t('You must enter a valid e-mail address.'));
  }
}

/**
 * Process the group contact form submission.
 */
function group_contact_form_submit($form, &$form_state) {
  global $language;
  $values = $form_state['values'];
  $group = entity_load_single('node', $values['gid']);

  // Get from and recipients email address.
  $from = $values['mail'];
  $recipients = group_contact_get_recipients($values['gid']);

  // Send the email.
  drupal_mail('group_contact', 'default', $recipients, $language, $values, $from);

  // Notify the user and redirect to group node.
  drupal_set_message(t('Your message has been sent.'));
  $form_state['redirect'] = ('node/' . $group->nid);
}

/**
 * Implements hook_mail().
 */
function group_contact_mail($key, &$message, $params) {
  $message['subject'] = $params['subject'];
  $message['body'] = $params['message']['value'];
}

/**
 * Format contact form recipient addresses
 */
function group_contact_get_recipients($gid) {
  $query = db_select('users', 'u');

  $query
    ->condition('u.uid', 0, '<>')
    ->condition('u.status', 1, '=')
    ->fields('u', array('uid', 'mail'))
    ->join('og_membership', 'ogm', "ogm.gid = :gid AND u.uid = ogm.etid AND ogm.entity_type = 'user'", array(':gid' => $gid));
  $members = $query->execute();

  $recipients = '';
  foreach ($members as $member) {
    $recipients .= (empty($recipients)) ? $member->mail : ', ' . $member->mail;
  }

  return $recipients;
}
